Converter 2.1
-------------

Input:
------
* WMOD files from Supremica 2.3.1


Outputs:
--------
* ST code (in a text file or within an existing TwinCAT project)
* Log of the console in a text file


New features:
-------------
* Support manually designed EFAs from Supremica
  - Converter 1.4 only supported EFA with automatically generated guards
* Possibility to generate ST code as standalone
  - Converter 1.4 generated ST code for TwinCAT projects
* Different options are accessible using flags when the script is started:
	-h, --help
					Displays all available commands

	-v,-V/--verbose,--VERBOSE
					Outputs additional information during the code generation

	-s/--standalone
					Paste the code into an output file “output.txt” instead of into a Twincat project.

	-e/--efa (experimental)
					Implements the wmod model as a normal EFA. Actions and guards are allowed. Some actions are not supported (e.g. +=). If guards are not implemented as integers ([0..XX]) but as discrete values (on, off, true, false, up, down,…), a DUT has to be added manually including these types. The code for the DUT will be generated automatically in the console.

	-a [Appendix]   (from Leonardo)
					An appendix can be inserted into all names (pous, outputs, inputs, …) to allow multiple pous in the same Twincat project. For example when controlling two stations with one RIOM.

				
Unsupported features:
---------------------
* Actions such as "X +=1"


Test:
-----					
* Laurin has performed basic tests (generating different files, building twincat solutions) but not run the code on a platform yet (also because he is lacking any meaningful EFA models).


-----
TODO:
-----
* Test more intensively this converter on various EFAs (expected from Leonardo and Nestor), and report bugs to Laurin Prenzel and Julien Provost